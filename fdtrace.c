#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <fcntl.h>
#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <sys/ptrace.h>
#include <asm/ptrace.h>
#include <sys/types.h>
#include <sys/syscall.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <unistd.h>
#include <assert.h>
#include <getopt.h>
#include <signal.h>

#define ARRAY_SIZE(x) (sizeof(x) / sizeof((x)[0]))

#include "drm-uapi/msm_drm.h"

enum {
      VERBOSE_SYSCALL	= 0x01,
      VERBOSE_IOCTL	= 0x02,
      VERBOSE_SUBMIT	= 0x04,
      VERBOSE_DECODE	= 0x08,
      VERBOSE_MISC	= 0x10
};

static int verbose_flags = 0;

static inline uint32_t
fui(float f)
{
	return (union { float f; uint32_t u; }) { .f = f }.u;
}

#include "adreno_pm4.xml.h"
#include "adreno_common.xml.h"
#include "a6xx.xml.h"

#include "syscalls.h"

static void
fail_if(int cond, const char *fmt, ...)
{
	va_list va;

	if (cond) {
		va_start(va, fmt);
		vfprintf(stderr, fmt, va);
		fprintf(stderr, "\n");
		va_end(va);
		exit(EXIT_FAILURE);
	}
}

static void
msg(uint32_t flag, const char *fmt, ...)
{
	va_list va;

	if (verbose_flags & flag) {
		va_start(va, fmt);
		vprintf(fmt, va);
	}
}

#define DRM_IOCTL_MSM_GEM_INFO_WO        DRM_IOW(DRM_COMMAND_BASE + DRM_MSM_GEM_INFO, struct drm_msm_gem_info)

static void *
xmalloc(size_t size)
{
	void *p = malloc(size);

	fail_if(p == NULL, "malloc failed");

	return p;
}

static int
xpread(int fd, void *buf, size_t count, off_t offset)
{
	int ret = pread(fd, buf, count, offset);

	fail_if(ret != count, "read failed: (ret = %d) %m", ret);

	return ret;
}

struct bo {
	uint64_t offset;
	uint64_t iova;
	uint64_t size;
	uint64_t child_address;
	uint64_t start, end;
} trace_bos[64];


static void
new_bo(const char *msg, uint32_t handle, uint64_t size)
{
	fail_if(handle >= ARRAY_SIZE(trace_bos), "%s new bo handle out of range", msg);

	trace_bos[handle].size = size;
}

static void
close_bo(const char *msg, uint32_t handle)
{
	trace_bos[handle].size = 0;
}

static void
check_handle(const char *msg, int handle)
{
	fail_if(handle >= ARRAY_SIZE(trace_bos), "%s handle out of range (%d)", msg, handle);
	fail_if(trace_bos[handle].size == 0, "%s invalid handle (%d)", msg, handle);
}

static void
handle_gem_new(pid_t child, int child_mem_fd, unsigned long args)
{
	struct drm_msm_gem_new gem_new, gem_new_out;
	int status;

	xpread(child_mem_fd, &gem_new, sizeof(gem_new), args);

	ptrace(PTRACE_SYSCALL, child, NULL, NULL);
	wait(&status);

	struct pt_regs regs;
	fail_if(ptrace(PTRACE_GETREGS, child, 0, &regs) != 0,
		"failed to get regs");
	fail_if(regs.ARM_r0 != 0, "DRM_IOCTL_MSM_GEM_NEW failed");

	xpread(child_mem_fd, &gem_new_out, sizeof(gem_new_out), args);

	new_bo("DRM_IOCTL_MSM_GEM_NEW", gem_new_out.handle, gem_new.size);
}

static void
handle_gem_info(pid_t child, int child_mem_fd, unsigned long args)
{
	struct drm_msm_gem_info gem_info, gem_info_out;
	int status;

	xpread(child_mem_fd, &gem_info, sizeof(gem_info), args);

	check_handle("DRM_IOCTL_MSM_GEM_INFO", gem_info.handle);

	ptrace(PTRACE_SYSCALL, child, NULL, NULL);
	wait(&status);

	xpread(child_mem_fd, &gem_info_out, sizeof(gem_info_out), args);

	switch (gem_info.info) {
	case MSM_INFO_GET_OFFSET:
		trace_bos[gem_info.handle].offset = gem_info_out.value;		
		break;
	case MSM_INFO_GET_IOVA:
		trace_bos[gem_info.handle].iova = gem_info_out.value;		
		break;
	}
}

static int
find_bo(uint64_t iova)
{
	int i;

	for (i = 0; i < ARRAY_SIZE(trace_bos); i++) {
		struct bo *bo = &trace_bos[i];

		if (bo->iova <= iova && iova < bo->iova + bo->size)
			return i;
	}

	return -1;
}

static void
update_bo_range(uint32_t handle, uint64_t start, uint32_t end)
{
	if (start < trace_bos[handle].start)
		trace_bos[handle].start = start;
	if (trace_bos[handle].end < end)
		trace_bos[handle].end = end;
}

static inline uint64_t
get_offset(const uint32_t *p)
{
	return (uint64_t) p[0] | ((uint64_t) p[1] << 32);
}

static void
mark_range(uint64_t iova, uint32_t length)
{
	uint64_t end;
	int i;

	i = find_bo(iova);
	fail_if(i == -1, "range [%llx+%d] not found\n", iova, length);

	struct bo *bo = &trace_bos[i];
	if (iova + length * 4 < bo->iova + bo->size)
		end = iova + length * 4;
	else
		end = bo->iova + bo->size;

	update_bo_range(i, iova, end);
}

#define FIELD(val, name) (((val) & name ## __MASK) >> name ## __SHIFT)

struct cp_load_state6 {
	uint32_t state_type;
	uint32_t state_src;
	uint32_t state_block;
	uint32_t num_unit;
	uint64_t address;
};

static inline struct cp_load_state6
decode_cp_load_state6(const uint32_t *dw)
{
	return (struct cp_load_state6) {
		.state_type	= FIELD(dw[1], CP_LOAD_STATE6_0_STATE_TYPE),
		.state_src	= FIELD(dw[1], CP_LOAD_STATE6_0_STATE_SRC),
		.state_block	= FIELD(dw[1], CP_LOAD_STATE6_0_STATE_BLOCK),
		.num_unit	= FIELD(dw[1], CP_LOAD_STATE6_0_NUM_UNIT),
		.address	= get_offset(&dw[2])
	};
}

static void
decode_ib(int child_mem_fd, uint64_t start, uint32_t length)
{
	uint32_t tessfactor[2] = { 0 };
	uint64_t tessfactor_addr;
	uint32_t size = length * 4;

	int handle = find_bo(start);
	fail_if(handle == -1, "range [%llx+%d] not found\n", start, size);

	update_bo_range(handle, start, start + length * 4);

	struct bo *bo = &trace_bos[handle];

	uint32_t *buffer = xmalloc(size);
	xpread(child_mem_fd, buffer, size, bo->child_address + (start - bo->iova));

	uint32_t *d = buffer;
	uint32_t *end = buffer + length;

	while (d < end) {
		uint32_t pkt = d[0] >> 28;
		uint32_t cnt = d[0] & 0x7f;

		switch (pkt) {
		case 7: {
			uint32_t opc = (d[0] >> 16) & 0x7f;
			switch(opc) {
			case CP_INDIRECT_BUFFER: {
				uint64_t ib_offset = get_offset(&d[1]);
				uint32_t ib_length = d[3];

				msg(VERBOSE_DECODE, "%-12s ib=0x%llx, length=%d\n",
				    get_pkt7_name(opc), ib_offset, ib_length);
				decode_ib(child_mem_fd, ib_offset, ib_length);
				break;
			}
			case CP_UNK_A6XX_55: {
				uint64_t ib_offset = get_offset(&d[1]);
				uint32_t ib_length = d[3] & 0xffff;

				msg(VERBOSE_DECODE, "%-12s ib=0x%llx, length=%d\n",
				    get_pkt7_name(opc), ib_offset, ib_length);
				decode_ib(child_mem_fd, ib_offset, ib_length);
				break;
			}
			case CP_SET_DRAW_STATE: {
				msg(VERBOSE_DECODE, "%-12s cnt=%d\n",
				    get_pkt7_name(opc), cnt / 3);
				for (int j = 0; j < cnt; j += 3) {
					uint32_t group_length = FIELD(d[1 + j], CP_SET_DRAW_STATE__0_COUNT);
					uint64_t group_offset = get_offset(&d[2 + j]);

					if (group_length > 0) {
						decode_ib(child_mem_fd, group_offset, group_length);
					}

				}
				break;
			}
			case CP_MEM_TO_REG: {
				uint64_t offset = get_offset(&d[2]);
				uint32_t cnt = FIELD(d[1], CP_MEM_TO_REG_0_CNT);
				msg(VERBOSE_DECODE, "%s\n", get_pkt7_name(opc));
				mark_range(offset, cnt);
				break;
			}
			case CP_LOAD_STATE6_FRAG:
			case CP_LOAD_STATE6_GEOM: {
				uint32_t num_dwords = 0;
				const struct cp_load_state6 state = decode_cp_load_state6(&d[0]);

				if (state.state_src != SS6_INDIRECT)
					break;

				if (state.state_type == ST6_CONSTANTS) {
					switch (state.state_block) {
					case SB6_VS_TEX:
					case SB6_HS_TEX:
					case SB6_DS_TEX:
					case SB6_GS_TEX:
					case SB6_FS_TEX:
					case SB6_CS_TEX:
						/* Textures, 16 dwords per sampler state */
						num_dwords = state.num_unit * 16;
						break;
					case SB6_VS_SHADER:
					case SB6_HS_SHADER:
					case SB6_DS_SHADER:
					case SB6_GS_SHADER:
					case SB6_FS_SHADER:
					case SB6_CS_SHADER:
						/* Constants, 4 dwords per unit */
						num_dwords = state.num_unit * 4;
						break;
					}
				} else {
					switch (state.state_block) {
					case SB6_VS_TEX:
					case SB6_HS_TEX:
					case SB6_DS_TEX:
					case SB6_GS_TEX:
					case SB6_FS_TEX:
					case SB6_CS_TEX:
						/* Samplers, 4 dwords per sampler state */
						num_dwords = state.num_unit * 4;
						break;
					case SB6_VS_SHADER:
					case SB6_HS_SHADER:
					case SB6_DS_SHADER:
					case SB6_GS_SHADER:
					case SB6_FS_SHADER:
					case SB6_CS_SHADER:
						/* Shaders, 32 dwords per unit */
						num_dwords = state.num_unit * 32;
						break;
					}
				}

				msg(VERBOSE_DECODE, "%-12s address=%llx, count=%d\n",
				    get_pkt7_name(opc), state.address, num_dwords);

				if (state.address != 0)
					mark_range(state.address, num_dwords);
				break;
			}
			default:
				msg(VERBOSE_DECODE, "%s\n", get_pkt7_name(opc));
				break;
			}
		}
		case 4: {
			uint32_t reg = (d[0] >> 8) & 0x3ffff;
			for (int j = 0; j < cnt; j++) {
				switch (reg + j) {
				case REG_A6XX_PC_TESSFACTOR_ADDR_LO:
					tessfactor[0] = d[1 + j];
					break;
				case REG_A6XX_PC_TESSFACTOR_ADDR_HI:
					tessfactor[1] = d[1 + j];
					break;
				}
			}
			break;
		}

		default:
			printf("unknown packet type %d (buffer %p, d %p, end %p)\n", pkt, buffer, d, end);
			break;
		}

		d += cnt + 1;
	}

	tessfactor_addr = get_offset(&tessfactor[0]);
	if (tessfactor_addr)
		mark_range(tessfactor_addr, 256);

	free(buffer);
}

static FILE *rd_out;

enum rd_sect_type {
	RD_NONE,
	RD_TEST,       /* ascii text */
	RD_CMD,        /* ascii text */
	RD_GPUADDR,    /* u32 gpuaddr, u32 size */
	RD_CONTEXT,    /* raw dump */
	RD_CMDSTREAM,  /* raw dump */
	RD_CMDSTREAM_ADDR, /* gpu addr of cmdstream */
	RD_PARAM,      /* u32 param_type, u32 param_val, u32 bitlen */
	RD_FLUSH,      /* empty, clear previous params */
	RD_PROGRAM,    /* shader program, raw dump */
	RD_VERT_SHADER,
	RD_FRAG_SHADER,
	RD_BUFFER_CONTENTS,
	RD_GPU_ID,
};

static void
rd_write_section(enum rd_sect_type type, const void *buf, int sz)
{
	fwrite(&type, 4, 1, rd_out);
	fwrite(&sz, 4, 1, rd_out);
	fwrite(buf, 1, sz, rd_out);
}

static void
snapshot_buf(int child_mem_fd, struct bo *bo)
{
	uint32_t size = bo->end - bo->start;
	uint32_t header[3] = {
		bo->start, size, bo->start >> 32,
	};
	rd_write_section(RD_GPUADDR, header, sizeof(header));

	uint32_t *buffer = xmalloc(size);
	xpread(child_mem_fd, buffer, size, bo->child_address + (bo->start - bo->iova));
	
	rd_write_section(RD_BUFFER_CONTENTS, buffer, size);

	free(buffer);
}


static void
handle_gem_submit(pid_t child, int child_mem_fd, unsigned long args)
{
	struct drm_msm_gem_submit submit;
	int status;

	xpread(child_mem_fd, &submit, sizeof(submit), args);
	msg(VERBOSE_SUBMIT,
	    "------ DRM_IOCTL_MSM_GEM_SUBMIT: %d bos, %d cmds\n",
	    submit.nr_bos, submit.nr_cmds);

	struct drm_msm_gem_submit_bo bos[64];
	fail_if(submit.nr_bos >= ARRAY_SIZE(bos), "too many bos");
	xpread(child_mem_fd, bos, submit.nr_bos * sizeof(bos[0]), submit.bos);

	struct drm_msm_gem_submit_cmd cmds[64];
	fail_if(submit.nr_cmds >= ARRAY_SIZE(cmds), "too many cmds");
	xpread(child_mem_fd, cmds, submit.nr_cmds * sizeof(cmds[0]), submit.cmds);

	for (unsigned int i = 0; i < submit.nr_bos; i++) {
		check_handle("DRM_IOCTL_MSM_GEM_SUBMIT", bos[i].handle);
		
		struct bo *bo = &trace_bos[bos[i].handle];
		bo->start = ~0ull;
		bo->end = 0;
	}

	for (unsigned int i = 0; i < submit.nr_cmds; i++) {
		msg(VERBOSE_SUBMIT,
		    "  cmd %2d: idx %d, offset %x, size %d, nr_relocs %d\n",
		    i, cmds[i].submit_idx, cmds[i].submit_offset, cmds[i].size, cmds[i].nr_relocs);
		fail_if(cmds[i].submit_idx >= submit.nr_bos, "DRM_IOCTL_MSM_GEM_SUBMIT: submit idx out of range");
		fail_if(cmds[i].nr_relocs != 0, "DRM_IOCTL_MSM_GEM_SUBMIT: relocs not handled");
		struct bo *bo = &trace_bos[bos[cmds[i].submit_idx].handle];
		msg(VERBOSE_SUBMIT,
		    "  bo handle: %d, size: %lld, iova: 0x%llx, child address: %llu (0x%llx)\n",
		    bos[cmds[i].submit_idx].handle, bo->size,
		    bo->iova, bo->child_address, bo->child_address);
		fail_if(bo->child_address == 0, "DRM_IOCTL_MSM_GEM_SUBMIT: no child address for cmd bo");

		decode_ib(child_mem_fd, bo->iova + cmds[i].submit_offset, cmds[i].size / 4);
	}

	uint32_t total_size = 0;
	uint32_t sparse_size = 0;
	for (uint32_t i = 0; i < submit.nr_bos; i++) {
		struct bo *bo = &trace_bos[bos[i].handle];

		if (bo->start != ~0ull) {
			fail_if(bo->end < bo->start, "bo range invalid");
			fail_if(bo->start < bo->iova, "bo range invalid");
			fail_if(bo->iova + bo->size < bo->end, "bo range invalid");
		}

		total_size += bo->size;
		if (bo->start < bo->end) {
			snapshot_buf(child_mem_fd, bo);
			sparse_size += bo->end - bo->start;
		}
	}

	for (uint32_t i = 0; i < submit.nr_cmds; i++) {
		struct bo *bo = &trace_bos[bos[cmds[i].submit_idx].handle];
		uint64_t iova = bo->iova + cmds[i].submit_offset;

		uint32_t header[3] = {
			iova, cmds[i].size / 4, iova >> 32
		};

		switch (cmds[i].type) {
		case MSM_SUBMIT_CMD_IB_TARGET_BUF:
			/* ignore IB-targets, we've logged the buffer, the
			 * parser tool will follow the IB based on the logged
			 * buffer/gpuaddr, so nothing more to do.
			 */
			break;
		case MSM_SUBMIT_CMD_CTX_RESTORE_BUF:
		case MSM_SUBMIT_CMD_BUF:
			rd_write_section(RD_CMDSTREAM_ADDR, header, sizeof(header));
			break;
		}
	}

	ptrace(PTRACE_SYSCALL, child, NULL, NULL);
	wait(&status);
}

static void
handle_gem_close(pid_t child, int child_mem_fd, unsigned long args)
{
	struct drm_gem_close gem_close;
	int status;

	xpread(child_mem_fd, &gem_close, sizeof(gem_close), args);
	close_bo("DRM_IOCTL_GEM_CLOSE", gem_close.handle);

	ptrace(PTRACE_SYSCALL, child, NULL, NULL);
	wait(&status);

}

static void
handle_prime_fd_to_handle(pid_t child, int child_mem_fd, unsigned long args)
{
	struct drm_prime_handle handle_out;
	int status;

	ptrace(PTRACE_SYSCALL, child, NULL, NULL);
	wait(&status);

	xpread(child_mem_fd, &handle_out, sizeof(handle_out), args);
	new_bo("DRM_IOCTL_PRIME_HANDLE_TO_FD", handle_out.handle, 1);
}

struct old_drm_msm_gem_info {
	__u32 handle;         /* in */
	__u32 info;           /* in - one of MSM_INFO_* */
	__u64 value;          /* in or out */
};

static void
handle_drm_ioctl(pid_t child, int child_mem_fd, unsigned long code, unsigned long args)
{
	int status;

	switch (code) {
	case DRM_IOCTL_MSM_GEM_INFO:
	case DRM_IOWR(DRM_COMMAND_BASE + DRM_MSM_GEM_INFO, struct old_drm_msm_gem_info):
		handle_gem_info(child, child_mem_fd, args);
		break;
	case DRM_IOCTL_MSM_GEM_NEW:
		handle_gem_new(child, child_mem_fd, args);
		break;
	case DRM_IOCTL_MSM_GEM_SUBMIT:
		handle_gem_submit(child, child_mem_fd, args);
		break;
	case DRM_IOCTL_MSM_GET_PARAM:
	case DRM_IOCTL_MSM_GEM_CPU_PREP:
	case DRM_IOCTL_MSM_GEM_CPU_FINI:
	case DRM_IOCTL_MSM_WAIT_FENCE:
	case DRM_IOCTL_MSM_GEM_MADVISE:
	case DRM_IOCTL_MSM_SUBMITQUEUE_NEW:
	case DRM_IOCTL_MSM_SUBMITQUEUE_CLOSE:
	case DRM_IOCTL_MSM_SUBMITQUEUE_QUERY:
	case DRM_IOCTL_PRIME_HANDLE_TO_FD:
	default:
		ptrace(PTRACE_SYSCALL, child, NULL, NULL);
		wait(&status);
		break;

	case DRM_IOCTL_GEM_CLOSE:
		handle_gem_close(child, child_mem_fd, args);
		break;

	case DRM_IOCTL_PRIME_FD_TO_HANDLE:
		handle_prime_fd_to_handle(child, child_mem_fd, args);
		break;
	}
}

static inline
uint32_t align_u32(uint32_t v, uint32_t a)
{
	return (v + a - 1) & ~(a - 1);
}

static const char rd_default_filename[] = "/tmp/fdtrace.rd";

static struct option long_options[] = {
	/* These options set a flag. */
	{ "verbose", optional_argument, 0, 'v' },
	{ "file",    required_argument, 0, 'o' },
	{ "help",    no_argument, 0, 'h' },
	{ 0, 0, 0, 0 }
};

static void
print_help(void)
{
	printf("usage: fdtrace [-o file] [--verbose [flags]] [--] PROG [ARGS]\n\n");
	printf("Verbose flags: syscall, ioctl, submit, decode, misc\n");
}

int
main(int argc, char *argv[])
{
	int c;
	const char *rd_filename = rd_default_filename;

	while (1) {
		int option_index = 0;

		c = getopt_long (argc, argv, "o:",
				 long_options, &option_index);

		if (c == -1)
			break;

		switch (c) {
		case 'h':
			print_help();
			return EXIT_SUCCESS;
		case 'o':
			rd_filename = optarg;
			break;
		case 'v':
			if (optarg) {
				if (strstr(optarg, "syscall"))
					verbose_flags |= VERBOSE_SYSCALL;
				if (strstr(optarg, "ioctl"))
					verbose_flags |= VERBOSE_IOCTL;
				if (strstr(optarg, "submit"))
					verbose_flags |= VERBOSE_SUBMIT;
				if (strstr(optarg, "decode"))
					verbose_flags |= VERBOSE_DECODE;
				if (strstr(optarg, "misc"))
					verbose_flags |= VERBOSE_MISC;
			} else {
				verbose_flags |= VERBOSE_MISC;
			}
			break;
		default:
			return EXIT_FAILURE;
		}
	}

	if (optind == argc) {
		printf("fdtrace: missing required PROG [ARGS]\n"
		       "see fdtrace --help\n");
		return EXIT_FAILURE;
	}

	rd_out = fopen(rd_filename, "wb");
	fail_if(rd_out == NULL, "filed to open rd file %s: %m", rd_filename);

	pid_t child = fork();
	if (child == 0) {
		ptrace(PTRACE_TRACEME, 0, NULL, NULL);
		kill(getpid(), SIGSTOP);
		execvp(argv[optind], argv + optind);
	}

	char cmd[256];
	int n;
	n = snprintf(cmd, sizeof(cmd), "tracing:");
	for (int i = optind; i < argc; i++)
		n += snprintf(cmd + n, sizeof(cmd) - n, " %s", argv[i]);
	n += snprintf(cmd + n, sizeof(cmd) - n, " (pid %d)\n", child);
	n += snprintf(cmd + n, sizeof(cmd) - n, "output to file %s\n", rd_filename);
	msg(VERBOSE_MISC, "%s", cmd);

	rd_write_section(RD_CMD, cmd, align_u32(n, 4));

	uint32_t gpu_id = 630;
	rd_write_section(RD_GPU_ID, &gpu_id, sizeof(gpu_id));

	ptrace(PTRACE_SETOPTIONS, child, 0, PTRACE_O_TRACESYSGOOD);

	char filename[256];
	snprintf(filename, sizeof(filename), "/proc/%d/mem", child);

	int drm_fd = -1;

	int status;
	while (1) {
		wait(&status);
		if (WIFEXITED(status))
			break;

		struct pt_regs regs;
		fail_if(ptrace(PTRACE_GETREGS, child, 0, &regs) != 0,
			"failed to get regs");
		int syscall_nr = regs.ARM_r7;

		switch (syscall_nr) {
		case SYS_openat: {
			/* We can only open and access /proc/$PID/mem
			 * while the process is stopped. */
			int child_mem_fd = open(filename, O_RDONLY);
			fail_if(child_mem_fd == -1, "failed to open %s: %m", filename);

			int ret;
			char data[256];
			ret = pread(child_mem_fd, data, sizeof(data), (unsigned) regs.ARM_r1);
			fail_if(ret == -1, "read %m");

			msg(VERBOSE_SYSCALL, "%-12s dirfd=%d, path=%s (0x%08x)\n",
			    get_syscall_name(syscall_nr), regs.ARM_r0, data, regs.ARM_r1);

			ptrace(PTRACE_SYSCALL, child, NULL, NULL);
			wait(&status);

			fail_if(ptrace(PTRACE_GETREGS, child, 0, &regs) != 0,
				"failed to get regs");
			int retval = regs.ARM_r0;

			if (strcmp(data, "/dev/dri/renderD128") == 0) {
				msg(VERBOSE_MISC, "open render node, fd=%d\n", retval);
				drm_fd = retval;
			} else if (strcmp(data, "/dev/dri/card1") == 0) {
				msg(VERBOSE_MISC, "open card1, fd=%d\n", retval);
				drm_fd = retval;
			}

			break;
		}

		case SYS_close: {
			msg(VERBOSE_SYSCALL, "%-12s fd=%d\n",
			    get_syscall_name(syscall_nr), regs.ARM_r0);

			if (regs.ARM_r0 == drm_fd) {
				msg(VERBOSE_MISC, "closing drm_fd %d\n", drm_fd);
				drm_fd = -1;
			}
			ptrace(PTRACE_SYSCALL, child, NULL, NULL);
			wait(&status);
			break;
		}

		case SYS_ioctl: {
			msg(VERBOSE_SYSCALL, "%-12s fd=%d, args=0x%08x\n",
			    get_syscall_name(syscall_nr), regs.ARM_r0, regs.ARM_r1);

			if (regs.ARM_r0 == drm_fd) {
				int child_mem_fd = open(filename, O_RDWR);
				fail_if(child_mem_fd == -1, "failed to open %s: %m", filename);

				handle_drm_ioctl(child, child_mem_fd, regs.ARM_r1,
						 regs.ARM_r2);
				close(child_mem_fd);
			}
			break;
		}

		case SYS_mmap2: {
			unsigned long length = regs.ARM_r1;
			int fd = regs.ARM_r4;
			uint64_t offset = (uint64_t) regs.ARM_r5 << 12;

			ptrace(PTRACE_SYSCALL, child, NULL, NULL);
			wait(&status);

			fail_if(ptrace(PTRACE_GETREGS, child, 0, &regs) != 0,
				"failed to get regs");

			msg(VERBOSE_SYSCALL, "%-12s addr=0x%lx, length=%d, prot=%d, flags=%d, fd=%d, offset=0x%lx\n",
			    get_syscall_name(syscall_nr),
			    regs.ARM_r0, regs.ARM_r1, regs.ARM_r2, regs.ARM_r3, regs.ARM_r4, regs.ARM_r5);

			if (fd != drm_fd)
				break;

			for (uint32_t i = 0; i < ARRAY_SIZE(trace_bos); i++) {
				if (trace_bos[i].size > 0 &&
				    trace_bos[i].offset == offset) {
					if (trace_bos[i].size == 1) {
						/* assume mmap length as bo size for prime imported bos */
						trace_bos[i].size = length;
					}
					fail_if(length > trace_bos[i].size, "mmap too big");
					trace_bos[i].child_address = (unsigned long) regs.ARM_r0;
					break;
				}
			}
			break;
		}

		case SYS_munmap: {
			fail_if(ptrace(PTRACE_GETREGS, child, 0, &regs) != 0,
				"failed to get regs");
			msg(VERBOSE_SYSCALL, "%-12s addr=0x%lx, length=%d\n",
			    get_syscall_name(syscall_nr), regs.ARM_r0, regs.ARM_r1);

			ptrace(PTRACE_SYSCALL, child, NULL, NULL);
			wait(&status);
			break;
		}

		default: {
			const char *name = get_syscall_name(syscall_nr);
			if (name == NULL)
				msg(VERBOSE_SYSCALL, "syscall 0x%x out of range (not handled)\n", syscall_nr);
			else
				msg(VERBOSE_SYSCALL, "%-12s (not handled)\n", name);

			ptrace(PTRACE_SYSCALL, child, NULL, NULL);
			wait(&status);
			break;
		}
		}

		if (WIFEXITED(status))
			break;

		ptrace(PTRACE_SYSCALL, child, NULL, NULL);
	}

	return 0;
}
