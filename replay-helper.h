#include <stdio.h>
#include <stdarg.h>
#include <stdint.h>
#include <string.h>
#include <signal.h>
#include <stdbool.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <sys/mman.h>
#include <assert.h>

#include "drm-uapi/msm_drm.h"

#define ARRAY_LENGTH(x) (sizeof(x) / sizeof((x)[0]))

#include "regs.h"

#define DIV_ROUND_UP(v, d) ( ((v) + (d) - 1) / (d) )

struct stream {
	uint64_t offset;
	uint32_t size;
};

struct allocator {
	int drm_fd;
	uint32_t bo_handle;
	uint64_t iova;
	void *data;
	uint32_t offset;
	uint32_t size;

#define NO_STREAM 1
	uint32_t stream_offset;
};

static void
fail_if(int cond, const char *fmt, ...)
{
	va_list va;

	if (cond) {
		va_start(va, fmt);
		vfprintf(stderr, fmt, va);
		fprintf(stderr, "\n");
		va_end(va);
		raise(SIGTRAP);
	}
}

static void
xioctl(int fd, long request, void *p)
{
	int ret = ioctl(fd, request, p);
	assert(ret == 0);
}

static inline void
init_allocator(struct allocator *cs, uint32_t size)
{
	cs->drm_fd = open("/dev/dri/renderD128", O_RDWR);

	struct drm_msm_gem_new gem_new = { .size = size, .flags = MSM_BO_WC };
	xioctl(cs->drm_fd, DRM_IOCTL_MSM_GEM_NEW, &gem_new);
	cs->bo_handle = gem_new.handle;
	cs->size = size;

	struct drm_msm_gem_info get_iova = {
		.handle = cs->bo_handle,
		.info = MSM_INFO_GET_IOVA,
	};
	xioctl(cs->drm_fd, DRM_IOCTL_MSM_GEM_INFO, &get_iova);
	cs->iova = get_iova.value;
	
	struct drm_msm_gem_info get_offset = {
		.handle = cs->bo_handle,
		.info = MSM_INFO_GET_OFFSET,
	};
	xioctl(cs->drm_fd, DRM_IOCTL_MSM_GEM_INFO, &get_offset);

	cs->data = mmap(NULL, size, PROT_READ | PROT_WRITE,
			MAP_SHARED, cs->drm_fd, get_offset.value);

	assert(cs->data != MAP_FAILED);

	cs->stream_offset = NO_STREAM;
	cs->offset = 0;
}

static inline void
reset_allocator(struct allocator *cs)
{
	cs->stream_offset = NO_STREAM;
	cs->offset = 0;
}

static inline void
submit_stream(struct allocator *cs, struct stream s)
{
	struct drm_msm_gem_submit_cmd cmd = {
		.type = MSM_SUBMIT_CMD_BUF,
		.submit_idx = 0,
		.submit_offset = s.offset - cs->iova,
		.size = s.size,
		.pad = 0,
		.nr_relocs = 0,
		.relocs = 0
	};

	struct drm_msm_gem_submit_bo bo = {
		.flags = MSM_SUBMIT_BO_READ | MSM_SUBMIT_BO_WRITE,
		.handle = cs->bo_handle,
		.presumed = cs->iova
	};

	struct drm_msm_gem_submit submit = {
		.flags = MSM_PIPE_3D0,
		.fence = 0,
		.nr_bos = 1,
		.nr_cmds = 1,
		.bos = (unsigned long) &bo,
		.cmds = (unsigned long) &cmd,
		.fence_fd = 0,
		.queueid = 0
	};

	xioctl(cs->drm_fd, DRM_IOCTL_MSM_GEM_SUBMIT, &submit);
}

static inline
uint32_t align_u32(uint32_t v, uint32_t a)
{
	return (v + a - 1) & ~(a - 1);
}

struct stream
allocate_block(struct allocator *cs, const void *contents, uint32_t size, uint32_t align)
{
	uint64_t offset = align_u32(cs->offset, align);

	assert(cs->stream_offset == NO_STREAM);
	assert(offset + size <= cs->size);

	cs->offset = offset + size;
	if (contents)
		memcpy(cs->data + offset, contents, size);

	return (struct stream) { .offset = cs->iova + offset, .size = size };
}

static inline void
begin_stream(struct allocator *cs, uint32_t align)
{
	assert(cs->stream_offset == NO_STREAM);

	cs->offset = align_u32(cs->offset, align);
	cs->stream_offset = cs->offset;
}

static inline struct stream
finish_stream(struct allocator *cs)
{
	assert(cs->stream_offset != NO_STREAM);

	struct stream s = {
		.offset = cs->iova + cs->stream_offset,
		.size = cs->offset - cs->stream_offset
	};

	cs->stream_offset = NO_STREAM;

	return s;
}

static inline void
emit_dw(struct allocator *cs, uint32_t dw)
{
	assert(cs->stream_offset != NO_STREAM);
	assert(cs->offset + 4 <= cs->size);

	*(uint32_t *)(cs->data + cs->offset) = dw;
	cs->offset += 4;
}

static inline unsigned
_odd_parity_bit(unsigned val)
{
	/* See: http://graphics.stanford.edu/~seander/bithacks.html#ParityParallel
	 * note that we want odd parity so 0x6996 is inverted.
	 */
	val ^= val >> 16;
	val ^= val >> 8;
	val ^= val >> 4;
	val &= 0xf;
	return (~0x6996 >> val) & 1;
}

static inline void
emit_pkt7(void *cs, uint32_t opc, uint32_t *v, uint32_t cnt)
{
	emit_dw(cs, CP_TYPE7_PKT | cnt |
		(_odd_parity_bit(cnt) << 15) |
		((opc & 0x7f) << 16) |
		((_odd_parity_bit(opc) << 23)));
	for (uint32_t i = 0; i < cnt; i++)
		emit_dw(cs, v[i]);
}

#define cp(cs, opc, ...) do {				\
		uint32_t v[] = { __VA_ARGS__ };		\
		emit_pkt7(cs, opc, v, ARRAY_LENGTH(v));	\
	} while (0)

static inline void
emit_pkt4(void *cs, uint16_t regindx, uint32_t *v, uint32_t cnt)
{
	emit_dw(cs, CP_TYPE4_PKT | cnt |
		(_odd_parity_bit(cnt) << 7) |
		((regindx & 0x3ffff) << 8) |
		((_odd_parity_bit(regindx) << 27)));
	for (uint32_t i = 0; i < cnt; i++)
		emit_dw(cs, v[i]);
}

#define wr(cs, reg, ...) do {				\
		uint32_t v[] = { __VA_ARGS__ };		\
		emit_pkt4(cs, reg, v, ARRAY_LENGTH(v));	\
	} while (0)

static inline void
ib(struct allocator *cs, struct stream s)
{
	cp(cs, CP_INDIRECT_BUFFER, s.offset, s.offset >> 32, s.size / 4);
}

#define group_id(g) ((g) << 24)
#define group_mask(m) ((m) << 20)

struct cp_set_draw_state_group {
	uint32_t	count;
	bool		dirty;
	bool		disable;
	bool		disable_all_groups;
	bool		load_immed;
	uint32_t	enable_mask;
	uint32_t	group_id;
};

static inline uint32_t
pack_cp_set_draw_state_group(struct cp_set_draw_state_group g)
{
	return
		g.count |
		(g.dirty << 16) |
		(g.disable << 17) |
		(g.disable_all_groups << 18) |
		(g.load_immed << 19) |
		(g.enable_mask << 20) |
		(g.group_id << 24);
}

#define group(dw0, s) ((dw0) | (s).size / 4), (s).offset, ((s).offset >> 32)
